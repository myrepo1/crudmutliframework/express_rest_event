require('dotenv/config');
const url = require('url');
const path = require('path');
const fs = require('fs');

let media_dir = process.env.MEDIA_DIR;
if(process.env.NODE_ENV==='test') {
    media_dir = process.env.TEST_MEDIA_DIR;
}

const checkDirExist = (dir) => {
    if(path.isAbsolute(dir)) {
        return fs.existsSync(dir);
    } else {
        const abs_path = path.join(process.env.PWD,media_dir,dir);
        return fs.existsSync(abs_path);
    }
};

const createDir = (dir) => {
    if(path.isAbsolute(dir)) {
        fs.mkdirSync(dir,{recursive:true});
    }else {
        const abs_path = path.join(process.env.PWD,media_dir,dir);
        fs.mkdirSync(abs_path, {recursive:true});
    }
};

const deleteDir = (dir) => {
    if(path.isAbsolute(dir)) {
        fs.rmdirSync(dir,{recursive:true});
    }else {
        const abs_path = path.join(process.env.PWD,media_dir,dir);
        fs.rmdirSync(abs_path,{recursive:true});
    }
}

const generateFullPath = (dir) => {
    if(path.isAbsolute(dir)) {
        return dir;
    } else {
        const abs_path = path.join(process.env.PWD,media_dir,dir);
        return abs_path;
    }
}

const generateFileURL = (req,filepath) => {
    const savePath = filepath.split('/').slice(1).join('/');
    return `${process.env.BASE_URL}/files/${savePath}`;
}

module.exports = {
    checkDirExist,
    createDir,
    deleteDir,
    generateFileURL,
    generateFullPath
};