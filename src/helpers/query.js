const checkFieldExist = (model,field)=>{
    return model.schema.path(field)===undefined?false:true;
};

const convertFieldQuery = (field,value) => {
    let result = {'$eq':value};
    const splitted_field = field.split('__');
    if(splitted_field.length===1) {
        return result;
    }
    const fieldOpr = splitted_field[1];
    switch (fieldOpr) {
        case 'gt':
            result = {'$gt':value};
            break;
        case 'gte':
            result = {'$gte':value};
            break;
        case 'lt':
            result = {'$lt':value};
            break;
        case 'lte':
            result = {'$lte':value};
            break;
        case 'like':
            result = {'$regex':value};
            break;
        case 'ilike':
            result = {'$regex':value,'$options':'i'};
            break;
    }
    return result;
};

const generateQuery = (model,query,schema) => {
    const result = {filter:{},reguler:{},error:null};
    const modelFields = {};
    const rawData = {};

    Object.keys(query).forEach((item)=>{
        const splitted_field = item.split('__');
        const field = splitted_field[0];
        if(checkFieldExist(model,field)) {
            if(modelFields[field]) {
                modelFields[field].push(item);
                rawData[field].push(query[item]);
            } else {
                modelFields[field] = [item,];
                rawData[field] = [query[item],];
            }
        } else {
            rawData[field] = query[field];
        }
    });
    
    const {error,value} = schema.validate(rawData,{abortEarly:false,stripUnknown:true});
    
    if(error) {
        result.error = error.details;
        return result;
    } else {
        Object.keys(value).map((item)=>{
            if(modelFields[item]) {
                const fields = modelFields[item];
                const values = value[item];
                fields.map((key,i)=>{
                    if(result.filter[item]) {
                        Object.assign(result.filter[item],convertFieldQuery(key,values[i]));
                    } else {
                        result.filter[item] = convertFieldQuery(key,values[i]);
                    }
                });
            } else {
                result.reguler[item] = value[item];
            }
        });
    }
    return result;
};

module.exports = generateQuery;