const successResponse = {
    status_code:200,
    status:true,
    message:'Request Success',
    data:null
};

const successResponseMany = {
    status_code:200,
    status:true,
    message:'Request Success.',
    data:[]
};

const successCreateResponse = {
    status_code:201,
    status:true,
    message:'Request has been created.',
    data:null
};

const badRequest = {
    status_code:400,
    status:false,
    message:'Data Validation Failed.',
    data:null,
};

const unauthenticatedRequet = {
    status_code:401,
    status:false,
    message:'Authentication Failed.',
    data:null
};

const unauthorizedRequest = {
    status_code:403,
    status:false,
    message:'Unauthorized Request.',
    data:null
};

const resourceNotFound = {
    status_code:404,
    status: false,
    message:'Resource Not Found.',
    data:null
};

const serverError = {
    status_code:500,
    status: false,
    message: 'Server Error',
    data: null
};

const responses = {
    200:successResponse,
    m200:successResponseMany,
    201:successCreateResponse,
    400:badRequest,
    401:unauthenticatedRequet,
    403:unauthorizedRequest,
    404:resourceNotFound,
    500:serverError
};

const generateResponse = (res,code,params={})=>{
    const response = {...responses[code],...params};
    return res.status(response.status_code).json(response);
};

module.exports = generateResponse;