const Joi = require('joi');

const generatePayloadSchema = (source,is_post=false)=>{
    const JoiSchema = {};
    Object.keys(source).map((item)=>{
        const data = source[item];
        if(data.is_payload) {
            let field = source[item].field;
            if(is_post && data.post_required) {
                field = field.required();
            }
            if(data.allowed_val) {
                data.allowed_val.forEach(element => {
                    field = field.allow(element);
                });
            }
            JoiSchema[item] = field;
        }
    });
    return Joi.object(JoiSchema);
};

const generateQuerySchema = (source) => {
    const JoiSchema = {};
    Object.keys(source).map((item)=>{
        const data = source[item];
        if(data.is_query) {
            let field = source[item].field;
            if(data.allowed_val) {
                data.allowed_val.forEach(element => {
                    field = field.allow(element);
                });
            }
            if(data.use_array) {
                field = Joi.array().items(field);
            }
            JoiSchema[item] = field;
        }
    });
    return Joi.object(JoiSchema);  
};

module.exports = {
    generatePayloadSchema,
    generateQuerySchema
};