require('dotenv/config');
const bcrypt = require('bcrypt');
const { not } = require('joi');
const jwt = require('jsonwebtoken');

const hashPassword = async (value) => {
    const hash = await bcrypt.hash(value,8);
    return hash;
};

const checkPassword = async (value,password) => {
    const result = await bcrypt.compare(value,password);
    return result;
};

const generateToken = (payload) => {
    const access_token = jwt.sign(
        payload,
        process.env.SECRET_KEY,
        {expiresIn:'1d'});
    return access_token;
};

const verifyToken = (raw_token) => {
    let identity = null;
    const token = raw_token.split(' ')[1];
    if (token) {
        jwt.verify(token,process.env.SECRET_KEY, (err,user)=>{
            if(err) {
                return;
            } else {
                identity = user;
            }
        });
    }
    return identity;
}

module.exports = {
    hashPassword,
    checkPassword,
    generateToken,
    verifyToken
};