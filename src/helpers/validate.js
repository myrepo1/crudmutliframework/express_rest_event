const appendError = (field,message,error_dest) => {
    const fieldName = field? field:'__all__';
    if(error_dest[fieldName]) {
        error_dest[fieldName].push(message);
    } else {
        error_dest[fieldName] = [message,];
    }
};

const convertError = (error_source,error_dest={})=>{
    error_source.forEach(element => {
        const label = element.context.label;
        const message = element.message.replace(/"/gm,"'");
        appendError(label,message,error_dest);
    });
};

const hasError = (errors) => {
    return Boolean(Object.keys(errors).length);
}

module.exports = {convertError,appendError,hasError};