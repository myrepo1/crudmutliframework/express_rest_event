require('dotenv/config');
const express = require("express");
const mongoose = require("mongoose");

const app = express();

let db = process.env.DB_URI;

if(process.env.NODE_ENV==='test') {
  db = process.env.TEST_DB_URI;
}
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Database Connected"))
  .catch((err) => console.log(err));

const accountRouter = require('./routes/account');
const eventRouter = require('./routes/event');
const fileRouter = require('./routes/file');

app.use('/accounts',accountRouter);
app.use('/events',eventRouter);
app.use('/files',fileRouter);

app.listen(process.env.PORT, () => console.log(`Server Running at ${process.env.PORT}`));

module.exports = app;