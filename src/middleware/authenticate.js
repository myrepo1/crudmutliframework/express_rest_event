const generateQuery = require('../helpers/query');
const generateResponse = require('../helpers/response');
const {verifyToken} = require('../helpers/security');

const useAuth = (req,res,next)=>{
    const authHeader = req.headers.authorization;
    if(authHeader) {
        const identity = verifyToken(authHeader,req);
        if (identity) {
            req.user = identity;
            return next();
        } else {
            return generateResponse(res,401);
        }
    } else {
        return generateResponse(res,401);
    }
};

const useAuthOptional = (req,res,next) => {
    const authHeader = req.headers.authorization;
    if(authHeader) {
        const identity = verifyToken(authHeader,req);
        if (identity) {
            req.user = identity;
            return next();
        } else {
            return generateResponse(res,401);
        }
    } else {
        req.user = null;
        return next();
    }
};

module.exports = {useAuth,useAuthOptional};