require('dotenv/config');
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

const {checkDirExist,generateFullPath,createDir} = require('../helpers/file');

let media_dir = process.env.MEDIA_DIR;
    if(process.env.NODE_ENV==='test') {
        media_dir = process.env.TEST_MEDIA_DIR;
    }

const genereateDiskStorage = (container)=>{
    const diskStorage = multer.diskStorage({
        destination: function (req,file,cb) {
            if(!checkDirExist(container)) {
                createDir(container);
            }
            cb(null, generateFullPath(container));
        },
        filename: function (req,file, cb) {
            const filename = crypto.randomBytes(12).toString('hex');
            cb(null, filename + path.extname(file.originalname));
        }
    });
    return diskStorage;
};

const generateFileMiddleware = (container) => {
    const diskStorage = genereateDiskStorage(container);
    return multer({storage:diskStorage}).single(container);
}

module.exports = generateFileMiddleware;