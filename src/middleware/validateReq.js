const generateResponse = require('../helpers/response');

const generateValidate = (validate) => {
    const useValidate = async (req,res,next) => {
        const result = await validate(req);
        if(result.status) {
            req.document = result.document;
            return next();
        } else {
            return generateResponse(res,result.code,result.response);
        }
    }
    return useValidate;
}

module.exports = generateValidate;