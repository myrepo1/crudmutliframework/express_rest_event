const { array } = require("joi");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transformAccount = (ret) => {
   ret.data_id = ret._id;
   delete ret._id;
   delete ret.password;
   delete ret.__v;
};

const AccountSchema = new Schema({
   username: {type: String, required:true},
   password: {type: String, required: true},
   name: {type: String, required: true},
   phone: {type: String, default: null},
   profpic: {type: String, default: null},
   is_staff: {type: Boolean, default: false},
   involve: [mongoose.Types.ObjectId]
},{
   toObject: {
      transform: function(doc,ret) {
         transformAccount(ret);
      }
   },
   toJSON: {
      transform: function(doc,ret) {
         transformAccount(ret);
      }
   },
});

module.exports = mongoose.model('account',AccountSchema);