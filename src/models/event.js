const mongoose = require("mongoose");
const {format} = require('date-fns');
const Schema = mongoose.Schema;

const transformEvent = (ret) =>{
    ret.data_id = ret._id;
    delete ret._id;
    delete ret.__v;
};

const transformEventJSON = (ret) => {
    const dateFormat = 'dd-MM-yyyy HH:mm';
    ret.data_id = ret._id;
    ret.start = format(ret.start,dateFormat);
    ret.end = ret.end?format(ret.end,dateFormat):null;
    delete ret._id;
    delete ret.__v;
    
};

const EventSchema = new Schema({
    name: {type: String, required:true},
    shortname: {type: String, default: null},
    start: {type: Date, required:true},
    end: {type: Date, default: null},
    location: {type:String, default:null},
    description: {type: String, default:null},
    preview: {type: String, default:null},
    involve: [mongoose.Types.ObjectId]
},{
    toObject:{
        transform: function (doc,ret) {
            transformEvent(ret);
        }
    },
    toJSON: {
        transform: function (doc,ret) {
            transformEventJSON(ret);
        }
    }
});

module.exports = mongoose.model('event',EventSchema);