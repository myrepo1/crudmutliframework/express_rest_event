const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Joi = require('joi');

const Account = require('../models/account');
const {generateFileURL} = require('../helpers/file');
const generateQuery = require('../helpers/query');
const generateResponse = require('../helpers/response');
const {generatePayloadSchema,generateQuerySchema} = require('../helpers/schema');
const {hashPassword,checkPassword, generateToken} = require('../helpers/security');
const {convertError,appendError,hasError} = require('../helpers/validate');
const {useAuth} = require('../middleware/authenticate');
const generateValidate = require('../middleware/validateReq');
const generateFileMiddleware = require('../middleware/upload');

const validationOptions = {abortEarly:false,stripUnknown:{arrays:true,objects:true}};

const AccountSchema = {
    username:{
        field:Joi.string(),
        is_payload:true,
        is_query:true,
        use_array:true,
        post_required:true
    },
    password:{
        field:Joi.string(),
        is_payload:true,
        is_query:true,
        use_array:true,
        post_required:true
    },
    name:{
        field:Joi.string(),
        is_payload:true,
        is_query:true,
        use_array:true,
        post_required:true
    },
    phone:{
        field:Joi.string().pattern(new RegExp('^[0-9]*$')),
        is_payload:true,
        is_query:true,
        use_array:true,
        allowed_val:[null,'']
    },
    is_staff:{
        field:Joi.boolean(),
        is_payload:true,
        is_query:true,
        use_array:true
    }
};

const AccountLoginSchema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required()
});

const validateUpload = async (req) => {
    const data_id = req.params.data_id;
    const user = req.user;
    const result = {response:{},status:false,document:null,code:null};
    if (user.id != data_id && !user.is_staff) {
        result.code = 401;
        return result;
    }
    try {
        const account = await Account.findOne({_id:data_id});
        if(!account) {
            result.code = 404;
            return result;
        }
        result.document = account;
        result.status = true;
        return result;
    } catch (err) {
        result.code = 500;
        return result;
    }
}

const jsonMW = [bodyParser.json(),];
const jsonAuthMW = [bodyParser.json(), useAuth];
const fileAuthMW = [useAuth,generateValidate(validateUpload),generateFileMiddleware('profpic')];

router.get('/', jsonAuthMW, async (req,res)=>{
    const user = req.user;
    const query = req.query;

    if(!user.is_staff) {
        return generateResponse(res,403);
    }
    const parsedQuery = generateQuery(Account,query,generateQuerySchema(AccountSchema));
    try {
        const data = await Account.find(parsedQuery.filter);
        return generateResponse(res,'m200',{data:data});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/', jsonMW, async (req,res)=>{
    const data = req.body;
    const generatedSchema = generatePayloadSchema(AccountSchema,is_post=true);
    const {error,value} = generatedSchema.validate(data,validationOptions);
    const detail_error = {}
    if(error) {
        convertError(error.details,detail_error);
        return generateResponse(res,400,{detail_message:detail_error});
    }
    try {
        const current_user = await Account.findOne({username:value.username});
        if(current_user) {
            appendError('username','Username already exist.',detail_error);
        }
    } catch (err) {
        return generateResponse(res,500);
    }
    if(hasError(detail_error)) {
        return generateResponse(res,400,{detail_message:detail_error});
    }

    try {
        value.password = await hashPassword(value.password);
        const account = new Account(value);
        const result = await account.save();
        return generateResponse(res,201,{data:result});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.get('/:data_id',jsonAuthMW, async (req,res)=>{
    const data_id = req.params.data_id;
    const user = req.user;
    if (user.id != data_id && !user.is_staff) {
        return generateResponse(res,403);
    }
    try {
        const account = await Account.findOne({_id:data_id});
        if(!account) {
            return generateResponse(res,404);
        }
        return generateResponse(res,200,{data:account});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.put('/:data_id', jsonAuthMW, async (req,res) =>{
    const data_id = req.params.data_id;
    const data = req.body;
    const user = req.user;
    const accountSchema = generatePayloadSchema(AccountSchema);
    if (user.id != data_id && !user.is_staff) {
        return generateResponse(res,403);
    }
    const detail_error = {}
    let account = null;
    const {error,value} = accountSchema.validate(data,validationOptions);
    if(error) {
        convertError(error.details,detail_error);
        return generateResponse(res,400,{detail_message:detail_error});
    }
    try {
        account = await Account.findOne({_id:data_id});
        if(!account) {
            return generateResponse(res,404);
        }
        if(value.username && account.username != value.username) {
            const current_user = await Account.findOne({username:value.username});
            if(current_user) {
                appendError('username','Username already exist.',detail_error);
            }
        }
        if(hasError(detail_error)) {
            return generateResponse(res,400,{detail_message:detail_error});
        }
    } catch (err) {
        return generateResponse(res,500);
    }

    try {
        if(value.password) {
            value.password = await hashPassword(value.password);
        }
        for(key in value) {
            account.set({[key]:value[key]})
        }
        await account.save();
        return generateResponse(res,200,{data:account});
    } catch (err) {
        const response = {...responses.serverError};
        return res.status(response.status_code).json(response);        
    }
});

router.delete('/:data_id', jsonAuthMW, async (req,res)=>{
    const data_id = req.params.data_id;
    const user = req.user;
    if (user.id != data_id && !user.is_staff) {
        return generateResponse(res,403);
    }
    
    try {
        const account = await Account.findOne({_id:data_id});
        if(!account) {
            return generateResponse(res,404);
        }
        const deleteAccount = await Account.deleteOne({_id:data_id});
        if(deleteAccount.ok === 1) {
            return generateResponse(res,200);
        }
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/:data_id/image', fileAuthMW, async (req, res)=>{
    try {
        const rawFilePath = req.file.path;
        if (!rawFilePath) {
            return generateResponse(res,400,{message:'File not found.'});
        }
        const account = req.document;
        account.profpic = generateFileURL(req,rawFilePath);
        await account.save();
        return generateResponse(res,200,{data:account});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/login', jsonMW, async (req,res)=>{
    const data = req.body;
    const {error,value} = AccountLoginSchema.validate(data,validationOptions);
    const detail_error = {}
    if(error) {
        convertError(error.details,detail_error);
        return generateResponse(res,400,{detail_message:detail_error});
    }
    let current_user = null;
    try {
        current_user = await Account.findOne({username:value.username});
        if(!current_user||!await checkPassword(value.password,current_user.password)) {
            appendError('__all__','Wrong username or password.',detail_error);
        }
        if(hasError(detail_error)) {
            return generateResponse(res,400,{detail_message:detail_error});
        }
    } catch (error) {
        return generateResponse(res,500);
    }
    const payload = {
        'id':current_user._id,
        'is_staff':current_user.is_staff
    };
    const access_token = generateToken(payload);
    const result = {...payload,access_token};
    return generateResponse(res,200,{data:result});
});

module.exports = router;