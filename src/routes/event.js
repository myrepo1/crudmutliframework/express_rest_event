const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Joi = require('joi');
const mongoose = require('mongoose');
const {parse} = require('date-fns');

const Event = require('../models/event');
const Account = require('../models/account');
const {generateFileURL} = require('../helpers/file');
const generateQuery = require('../helpers/query');
const generateResponse = require('../helpers/response');
const {generatePayloadSchema,generateQuerySchema} = require('../helpers/schema');
const {convertError,appendError,hasError} = require('../helpers/validate');
const {useAuth,useAuthOptional} = require('../middleware/authenticate');
const generateFileMiddleware = require('../middleware/upload');
const generateValidate = require('../middleware/validateReq');

const validationOptions = {abortEarly:false,stripUnknown:true};

const dateValidator = (value) =>{
    const dateFormat = 'dd-MM-yyyy HH:mm';
    const parsedDate = parse(value,dateFormat, new Date());
    return parsedDate.getDate()?parsedDate:false;
};

const EventSchema = {
    name:{
        field: Joi.string(),
        is_payload:true,
        is_query:true,
        use_array:true,
        post_required:true
    },
    shortname:{
        field:Joi.string(),
        is_payload:true,
        use_array:true,
        post_required:true
    },
    start:{
        field:Joi.string().custom((value,helpers)=>{
            const newValue = dateValidator(value);
            if(newValue) {
                return newValue;
            } else {
                throw new Error();
            }
        }).messages({'any.custom':'Unable to parse value.'}),
        is_payload:true,
        is_query:true,
        post_required:true,
        use_array:true
    },
    end:{
        field:Joi.string().custom((value,helpers)=>{
            const newValue = dateValidator(value);
            if(newValue) {
                return newValue;
            } else {
                throw new Error();
            }
        }).messages({'any.custom':'Unable to parse value.'}),
        is_payload:true,
        is_query:true,
        allowed_val:[null,],
        use_array:true
    },
    description:{
        field:Joi.string(),
        is_payload:true,
        is_query:true,
        allowed_val:[null,''],
        use_array:true
    },
    location:{
        field:Joi.string(),
        is_payload:true,
        is_query:true,
        allowed_val:[null,''],
        use_array:true
    }
};

const addPreviewValidation = async (req) => {
    const result = {document:null,status:false,response:null,code:null};
    const data_id = req.params.data_id;
    const user = req.user;
    if(!user.is_staff) {
        result.code = 403;
        return result;
    }
    try {
        const event = await Event.findById(data_id);
        if(!event) {
            result.code = 404;
            return result;
        }
        result.document = event;
        result.status = true;
    } catch (err) {
        result.code = 500;
        return result;
    }
    return result;
};

const jsonMW = [bodyParser.json(),];
const jsonAuthMW = [bodyParser.json(), useAuth];
const fileAuthMW = [useAuth,generateValidate(addPreviewValidation),generateFileMiddleware('preview')];

router.get('/', jsonMW, async (req,res)=>{
    const query = req.query;
    const parsedQuery = generateQuery(Event,query,generateQuerySchema(EventSchema));
    try {
        const events = await Event.find(parsedQuery.filter);
        return generateResponse(res,'m200',{data:events});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/', jsonAuthMW, async (req,res)=>{
    const user = req.user;
    if(!user.is_staff) {
        return generateResponse(res,403);
    }
    const data = req.body;
    const generatedSchema = generatePayloadSchema(EventSchema,is_post=true);
    const {error,value} = generatedSchema.validate(data,validationOptions);
    const detail_error = {};
    if(error) {
        convertError(error.details,detail_error);
        return generateResponse(res,400,{detail_message:detail_error});
    }
    try {
        const current_event = await Event.findOne({name:value.name});
        if(current_event) {
            appendError('name',`Event with name ${value.name} already exist.`,detail_error);
        }
    } catch (err) {
        return generateResponse(res,500);
    }

    if(hasError(detail_error)) {
        return generateResponse(res,400,{detail_message:detail_error});
    }
    if(value.end && value.end < value.start) {
        appendError(null,'Start must be before end.',detail_error);
    }
    if(hasError(detail_error)) {
        return generateResponse(res,400,{detail_message:detail_error});
    }
    try {
        const event = new Event(value);
        await event.save();
        return generateResponse(res,201,{data:event});
    } catch (err) {
        const response = {...responses.serverError};
        return res.status(response.status_code).json(response);
    }
});

router.get('/:data_id', jsonMW, async (req,res)=>{
    const data_id = req.params.data_id;
    try {
        const event = await Event.findById(data_id);
        if(!event) {
            return generateResponse(res,404);
        }
        return generateResponse(res,200,{data:event});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.put('/:data_id', jsonAuthMW, async (req,res) => {
    const data_id = req.params.data_id;
    const user = req.user;
    const data = req.body;
    if(!user.is_staff) {
        return generateResponse(res,403);
    }
    let event = null;
    const detail_error = {};
    const generatedSchema = generatePayloadSchema(EventSchema);
    const {error,value} = generatedSchema.validate(data,validationOptions);
    if(error) {
        convertError(error.details,detail_error);
        return generateResponse(res,400,{detail_message:detail_error});
    }
    try {
        event = await Event.findById(data_id);
        if(!event) {
            return generateResponse(res,404);
        }
        if(value.name && value.name != event.name && await Event.findOne({name:value.name})) {
            appendError('name',`Event with name ${value.name} already exist.`,detail_error);
        }
    } catch (err) {
        return generateResponse(res,500);
    }
    if(hasError(detail_error)) {
        return generateResponse(res,400,{detail_message:detail_error});
    }

    if((value.start && value.start != event.start) || (value.end && value.end != event.end)) {
        const checkStart = value.start?value.start:event.start;
        const checkEnd = value.end?value.end:event.end;
        if(checkEnd && checkStart > checkEnd) {
            appendError(null,'Start must be before end.',detail_error);
        }
    }
    if(hasError(detail_error)) {
        return generateResponse(res,400,{detail_message:detail_error});
    }

    try {
        for(key in value) {
            event.set({[key]:value[key]});
        }
        await event.save();
        return generateResponse(res,200,{data:event});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.delete('/:data_id', jsonAuthMW, async (req,res)=>{
    const data_id = req.params.data_id;
    const user = req.user; 
    if(!user.is_staff) {
        return generateResponse(res,403);
    }

    try {
        const event = await Event.findById(data_id);
        if(!event) {
            return generateResponse(res,404);
        }
        const deleteEvent = await Event.deleteOne({_id:data_id});
        if(deleteEvent.ok === 1) {
            return generateResponse(res,200);
        }
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/:data_id/image', fileAuthMW, async (req,res)=>{
    try {
        const rawFilePath = req.file.path;
        if (!rawFilePath) {
            return generateResponse(res,400,{message:'File not found.'});
        }
        const event = req.document;
        event.preview = generateFileURL(req,rawFilePath);
        await event.save();
        return generateResponse(res,200,{data:event});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/:data_id/join',jsonAuthMW, async (req,res)=>{
    const data_id = req.params.data_id;
    const user = req.user;
    try {
        const event = await Event.findOne({_id:data_id,involve:{'$ne':mongoose.Types.ObjectId(user.id)}});
        const account = await Account.findById(user.id);
        if(!event || !account) {
            return generateResponse(res,404);
        }
        const detail_error = {};
        const now = new Date();
        if(event.start<now) {
            appendError(null,'Event has passed.',detail_error);
        }
        if(hasError(detail_error)) {
            return generateResponse(res,400,{detail_message:detail_error});
        }
        event.involve.push(user.id);
        account.involve.push(event._id);
        await event.save();
        await account.save();
        return generateResponse(res,200,{data:event});
    } catch (err) {
        return generateResponse(res,500);
    }
});

router.post('/:data_id/cancel', jsonAuthMW, async (req,res)=>{
    const data_id = req.params.data_id;
    const user = req.user;
    try {
        const event = await Event.findOne({_id:data_id,involve:mongoose.Types.ObjectId(user.id)});
        const account = await Account.findById(user.id);
        if(!event || !account) {
            return generateResponse(res,404);
        }
        const detail_error = {};
        const now = new Date();
        if(event.start<now) {
            appendError(null,'Event has passed.',detail_error);
        }
        if(hasError(detail_error)) {
            return generateResponse(res,400,{detail_message:detail_error});
        }
        event.involve.pull(user.id);
        account.involve.pull(event._id);
        await event.save();
        await account.save();
        return generateResponse(res,200,{data:event});
    } catch (err) {
        return generateResponse(res,500);
    }
});

module.exports = router;