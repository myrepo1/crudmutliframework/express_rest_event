require('dotenv/config');
const express = require('express');
const router = express.Router();

const responses = require('../helpers/response');
const {checkDirExist,generateFullPath} = require('../helpers/file');

router.get('/:container/:filename', async (req,res)=>{
    const container = req.params.container;
    const filename = req.params.filename;
    const dir = generateFullPath(`${container}/${filename}`);
    if(checkDirExist(dir)) {
        res.sendFile(dir);
    } else {
        const response = responses.resourceNotFound;
        return req.status(response.status_code).json(response);
    }
});

module.exports = router;