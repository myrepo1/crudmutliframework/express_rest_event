require('dotenv/config');
const path = require('path');
const crypto = require('crypto');
const fs = require('fs');
const mongoose = require("mongoose");
const {addDays,addHours} = require('date-fns');

const Account = require('../models/account');
const Event = require('../models/event');
const {hashPassword} = require('../helpers/security');
const {checkDirExist,createDir,deleteDir,generateFullPath} = require('../helpers/file');

const filesEndPoint = process.env.BASE_URL + '/files';
const now = new Date();
now.setSeconds(0);
now.setMilliseconds(0);

const seed_data = {
    accounts: [
        {
            username: 'admin',
            password: 'admin321_123',
            is_staff: true,
            name: 'Event Admin',
            phone: '08098912312'
        },{
            username: 'person1',
            password: 'person_321',
            name: 'Budi',
            phone: '12930123213',
            is_staff: false
        }
    ],
    events: [
        {
            name: 'Bazaar Rame Tahunan',
            shortname: 'bazaar20',
            start: addDays(now,5),
            end: addDays(now,8),
            description: 'Event bazaar tahunan. Banyak jualan disini.',
            location: 'Gedung Sate'
        },{
            name: 'Theater Perjuangan Kemerdekaan 1945',
            shortname: 'TP45',
            start: addDays(now,4),
            end: addHours(addDays(now,4),2),
            description: 'Pertunjukan proses perjuangan kemerdekaan.',
            location: 'Gedung Merdeka'
        },{
            name: 'Lomba lari marathon se Bandung',
            shortname: 'maraBDG',
            start: addDays(now,-5),
            end: addHours(addDays(now,-5), 5),
            description: 'Lomba lari marathon se bandung.',
            location: 'Jalan Merdeka'
        },{
            name: 'Layar Tancap Bandung Tempo Doeloe',
            shortname: 'oldBDG',
            start: addDays(now, 3),
            end: addHours(addDays(now, 3), 3),
            description: 'Nonton bareng gambaran Bandung tempo dulu.',
            location: 'Museum Sri Baduga'
        },{
            name: '1000 Pohon hijaukan Negeri',
            shortname: '1000hijau',
            start: addDays(now, 10),
            description: 'Menanam pohon bersama untuk penghijauan.',
            location: 'Babakan Siliwangi'
        }
    ]
};

const promiseCopyFile = (...args) => {
    return new Promise((resolve,reject)=>fs.copyFile(...args,(err)=>{
        if(err) {
            reject(err);
        } else {
            resolve();
        }
    }));
}

const create_account = async (data,profpic)=>{
    data.password = await hashPassword(data.password);
    const extname = profpic.split('.').pop();
    const fileName = crypto.randomBytes(12).toString('hex');
    await promiseCopyFile(profpic,generateFullPath(`profpic/${fileName}.${extname}`));
    data.profpic = filesEndPoint + `/profpic/${fileName}.${extname}`;
    const account = new Account(data);
    await account.save();
    return account;
};

const create_event = async (data,preview) => {
    const event = new Event(data);
    const extname = preview.split('.').pop();
    const fileName = crypto.randomBytes(12).toString('hex');
    await promiseCopyFile(preview,generateFullPath(`preview/${fileName}.${extname}`));
    data.preview = filesEndPoint + `/preview/${fileName}.${extname}`;
    await event.save();
    return event;
}

const seed_account = async (collections,profpic) => {
    console.log('Begin Seed Account');
    const collection = Account.collection.collectionName;
    if(collections.indexOf(collection) !== -1) {
        await mongoose.connection.db.dropCollection(collection);
    }
    await Promise.all(seed_data.accounts.map((data)=>create_account(data,profpic)));
    console.log('End Seed Account');
}

const seed_event = async (collections,preview) => {
    console.log('Begin Seed Event');
    const collection = Event.collection.collectionName;
    if(collections.indexOf(collection) !== -1) {
        await mongoose.connection.db.dropCollection(collection);
    }
    await Promise.all(seed_data.events.map((data)=>create_event(data,preview)));
    console.log('End Seed Event');
};

const prepare_seed = (seed_static={}) => {
    if(checkDirExist('profpic')) {
        deleteDir('profpic');
    }
    createDir('profpic');
    if(checkDirExist('preview')) {
        deleteDir('preview')
    }
    createDir('preview');
    const previewPath = path.join(process.env.PWD, 'src','seeder','static','preview.jpeg');
    const profpicPath = path.join(process.env.PWD, 'src','seeder','static','profpic.jpeg');
    if(!checkDirExist(previewPath)) {
        console.log('Preview Image Not Found.');
        return false;
    }
    if(!checkDirExist(profpicPath)) {
        console.log('Profpic Image Not Found.');
        return false;
    }
    seed_static.preview = previewPath;
    seed_static.profpic = profpicPath;
    return true;
};

const seed = async ()=>{
    console.log('Begin Seeding');
    const db = process.env.DB_URI;
    const seed_static = {}
    try {
        if(!prepare_seed(seed_static)) {
            process.exit();
        }; 
        await mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true });
        const collections = (await mongoose.connection.db.listCollections().toArray()).map(collection => collection.name);
        await seed_account(collections,seed_static.profpic);
        await seed_event(collections,seed_static.preview);
        console.log('End Seeding');
        process.exit();
    } catch (err) {
        console.log('Seeding Failed.')
        console.log(err);
        process.exit();
    }
};

seed();