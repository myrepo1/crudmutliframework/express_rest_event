const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount,createToken,getImage,clearMedia} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Add Profpic', function(){
    let account = null;
    let token = null;

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
            clearMedia();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should add profpic', async function() {
        const url = `/accounts/${account.account.data_id}/image`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token})
        .attach('profpic',getImage('profpic'),'profpic.jpeg');

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('profpic');
        data.should.not.have.property('password');
        let validURL = true;
        try {
            new URL(data.profpic);
        } catch (TypeError) {
            validURL = false;
        }
        validURL.should.be.true;
    });

    it('It should not add profpic when account not found', async function() {
        const id = mongoose.Types.ObjectId();
        const url = `/accounts/${id.toHexString()}/image`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token})
        .attach('profpic',getImage('profpic'),'profpic.jpeg');

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });

    it('It should not add profpic when no token provided', async function() {
        const url = `/accounts/${account.account.data_id}/image`;

        const res = await chai.request(app)
        .post(url)
        .attach('profpic',getImage('profpic'),'profpic.jpeg');

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });
});