const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Post', function() {
    afterEach(async function() {
        try {
            await Account.collection.drop();
        } catch (err) {
            return;
        }
    });

    it('It should create account', async function(){
        const payload = {
            username: 'test_account',
            password: 'test123',
            name: 'dummy_account',
            phone: '123123123'
        };
        const res = await chai.request(app)
        .post('/accounts')
        .send(payload);

        res.should.have.status(201);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(201);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('username');
        data.should.have.property('name');
        data.should.not.have.property('password');
        data.username.should.equal(payload.username);
        data.name.should.equal(payload.name);
        data.is_staff.should.equal(false);
        let is_account = false;
        const created_account = await Account.findOne({username:payload.username});
        if(created_account) {
            is_account = true;
        }
        expect(is_account).to.be.true;
    });

    it('It should not create account when required fields are empty.', async function(){
        const payload = {
            username: '',
            password: '',
            name: ''
        };

        const res = await chai.request(app)
        .post('/accounts')
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status.should.equal(false);
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('username');
        error.should.have.property('password');
        error.should.have.property('name');
        expect(error.username).to.include("'username' is not allowed to be empty");
        expect(error.password).to.include("'password' is not allowed to be empty");
        expect(error.name).to.include("'name' is not allowed to be empty");

        let is_account = false;
        const created_account = await Account.findOne({username:payload.username});
        if(created_account) {
            is_account = true;
        }
        expect(is_account).to.be.false;
    });

    it('It should not create account when value is invalid.', async function() {
        const payload = {
            username: 'asda123',
            password: 'asdasd12',
            name: 'asdasd',
            phone: 'aksd12312'
        };

        const res = await chai.request(app)
        .post('/accounts')
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status.should.equal(false);
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('phone');
        expect(res.body.detail_message.phone)
        .to.include(`'phone' with value '${payload.phone}' fails to match the required pattern: /^[0-9]*$/`);

        let is_account = false;
        const created_account = await Account.findOne({username:payload.username});
        if(created_account) {
            is_account = true;
        }
        expect(is_account).to.be.false;
    });

    it('It should not create account when username already exist.', async function() {
        const new_account = await createAccount();
        const payload = {
            username: new_account.account.username,
            password: 'asdasd12',
            name: 'asdasd',
            phone: '0123123'
        };

        const res = await chai.request(app)
        .post('/accounts')
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status.should.equal(false);
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        res.body.detail_message.should.have.property('username');
        expect(res.body.detail_message.username).to.include('Username already exist.');

        let is_account = false;
        const created_account = await Account.findOne({name:payload.name});
        if(created_account) {
            is_account = true;
        }
        expect(is_account).to.be.false;
    });
});