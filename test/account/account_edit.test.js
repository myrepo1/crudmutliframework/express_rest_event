const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount,createToken} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Edit', function() {
    let account = null;
    let token = null;
    
    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should edit account', async function() {
        const url = `/accounts/${account.account.data_id}`;
        const payload = {
            name:'changed'
        };
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('name');
        data.should.not.have.property('password');
        data.name.should.equal(payload.name);

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.equal(payload.name);
    });

    it('It should not edit account when no token provided.', async function() {
        const url = `/accounts/${account.account.data_id}`;
        const payload = {
            name:'changed'
        };
        const res = await chai.request(app)
        .put(url)
        .send(payload);

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.not.equal(payload.name);
    });

    it('It should not edit account when account is not found.', async function() {
        const id = mongoose.Types.ObjectId();
        const url = `/accounts/${id.toHexString()}`;
        const payload = {
            name:'changed'
        };
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.not.equal(payload.name);
    });

    it('It should not edit account when field is empty.', async function () {
        const url = `/accounts/${account.account.data_id}`;
        const payload = {
            username:'',
            name:''
        };
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('username');
        error.should.have.property('name');
        expect(error.username).to.include("'username' is not allowed to be empty");
        expect(error.name).to.include("'name' is not allowed to be empty");

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.not.equal(payload.name);
    });

    it('It should not create account when value is invalid.', async function() {
        const url = `/accounts/${account.account.data_id}`;
        const payload = {
            phone:'123klasdasd'
        };
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        res.body.detail_message.should.have.property('phone');
        expect(res.body.detail_message.phone)
        .to.include(`'phone' with value '${payload.phone}' fails to match the required pattern: /^[0-9]*$/`);

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.not.equal(payload.phone);
    });

    it('It should not update account when username already exist.', async function(){
        const new_account = {
            username:'newAccount',
            password:'asdasdasd',
            name:'New Account'
        };
        await createAccount(new_account);
        const url = `/accounts/${account.account.data_id}`;
        const payload = {
            username:new_account.username
        };
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        res.body.detail_message.should.have.property('username');
        expect(res.body.detail_message.username).to.include('Username already exist.');

        const current_account = await Account.findById(account.account.data_id);
        current_account.name.should.not.equal(payload.username);
    });
});