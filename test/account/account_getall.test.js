const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount,createToken} = require('../test_helper');

const should = chai.should();

chai.use(chaiHttp);
describe('Account Get All', function () {
    let account = null;
    let token = null;
    
    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get all accounts', async function() {
        const res = await chai.request(app)
        .get('/accounts')
        .set({'Authorization':token});

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        const data = res.body.data;
        data.should.be.a('array');
        data[0].should.have.property('data_id');
        data[0].should.have.property('username');
        data[0].should.not.have.property('password');
        data[0].username.should.equal(account.account.username);
    });

    it('It should failed get accounts when no authentication', async function() {
        const res = await chai.request(app).get('/accounts');

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(401);
        res.body.status.should.equal(false);
    });

    it('It should failed get accounts when wrong token format', async function() {
        const res = await chai.request(app)
        .get('/accounts')
        .set({'Authorization':'askdalsdasd'});

        res.should.have.status(401);
        res.should.be.json;
        res.body.status_code.should.equal(401);
    });
});

describe('Account Get All with query', function(){
    let account = null;
    let account1 = null;
    let token = null;
    
    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
            const moreAccountData = {
                username:'test123',
                password:'abcde',
                name:'More Account',
                phone:'123121231',
                is_staff:false
            };
            account1 = await createAccount(moreAccountData);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
        } catch (err) {
            console.log(err);
        }
    }); 

    it('It should get all some accounts with query equal', async function() {
        const res = await chai.request(app)
        .get('/accounts')
        .query({username:'test'})
        .set({'Authorization':token});

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('data');
        const data = res.body.data;
        data.should.be.a('array');
        data.length.should.equal(1);
        data[0].should.have.property('username');
        data[0].username.should.equal(account.account.username);
    });

    it('It should get some accounts with query ilike',async function(){
        const res = await chai.request(app)
        .get('/accounts')
        .query({username__ilike:'test'})
        .set({'Authorization':token});

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('data');
        const data = res.body.data;
        data.should.be.a('array');
        data.length.should.equal(2);
        data[0].should.have.property('username');
        data[0].username.should.equal(account.account.username);
        data[1].username.should.equal(account1.account.username);
    });
});