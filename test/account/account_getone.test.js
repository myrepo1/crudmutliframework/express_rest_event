const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount,createToken} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Get One', function() {
    let account = null;
    let token = null;

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get account', async function() {
        const url = `/accounts/${account.account.data_id}`;
        const res = await chai.request(app)
        .get(url)
        .set({'Authorization':token});

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('name');
        data.should.not.have.property('password');
        data.username.should.equal(account.account.username);
    });

    it('It should not get account when no token provided', async function () {
        const url = `/accounts/${account.account.data_id}`;
        const res = await chai.request(app)
        .get(url);

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });

    it('It should not get account when account is not found.', async function(){
        const id = mongoose.Types.ObjectId();
        const url = `/accounts/${id.toHexString()}`;
        
        const res = await chai.request(app)
        .get(url)
        .set({'Authorization':token});

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });
});