const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


const app = require('../../src/index');
const Account = require('../../src/models/account');
const {createAccount,createToken,clearMedia} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Get Profpic', function(){
    let account = null;

    beforeEach(async function() {
        try {
            account = await createAccount(null,true);
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Account.collection.drop();
            clearMedia();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get profpic', async function(){
        const url = account.account.profpic;
        const res = await chai.request(url).get('/');
        res.should.have.status(200);
        res.type.should.equal('image/jpeg');
    });
});