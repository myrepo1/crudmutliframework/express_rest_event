const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


const app = require('../../src/index');
const Event = require('../../src/models/event');
const Account = require('../../src/models/account');
const {createAccount,createToken,createEvent,getImage,clearMedia} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Event Add Preview', function(){
    let account = null;
    let token = null;
    let event = null;

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
            event = await createEvent();
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Event.collection.drop();
            await Account.collection.drop();
            clearMedia();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should add preview', async function(){
        const url = `/events/${event.event.data_id}/image`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token})
        .attach('preview',getImage('preview'),'preview.jpeg');
        
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('preview');
        let validURL = true;
        try {
            new URL(data.preview);
        } catch (TypeError) {
            validURL = false;
        }
        validURL.should.be.true;
    });

    it('It should not add preview when no token is provided.', async function(){
        const url = `/events/${event.event.data_id}/image`;

        const res = await chai.request(app)
        .post(url)
        .attach('preview',getImage('preview'),'preview.jpeg');

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });

    it('It should not add preview when using non staff account.', async function(){
        const newAccountData = {
            username:'newAccount',
            password:'123123',
            name:'New Account'
        };
        const newAccount = await createAccount(newAccountData);
        const newToken = createToken(newAccount.account);

        const url = `/events/${event.event.data_id}/image`;
        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':newToken})
        .attach('preview',getImage('preview'),'preview.jpeg');

        res.should.have.status(403);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(403);
    });

    it('It should not add preview when event is not found.', async function(){
        const id = mongoose.Types.ObjectId();
        const url = `/events/${id.toHexString()}/image`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token})
        .attach('preview',getImage('preview'),'preview.jpeg');
        
        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });
});