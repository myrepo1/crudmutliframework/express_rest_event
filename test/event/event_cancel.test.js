const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const {addDays} = require('date-fns');


const app = require('../../src/index');
const Event = require('../../src/models/event');
const Account = require('../../src/models/account');
const {createAccount,createToken,createEvent} = require('../test_helper');
const { events } = require('../../src/models/event');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Account Cancel From an Event', function(){
    let account = null;
    let token = null;
    let event = null;

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
            const now = new Date();
            const eventData = {
                name:'Test Event',
                shortname:'te123',
                start: addDays(now,3),
                end: addDays(now,4),
                description: 'This is test event',
                involve: [account.account.data_id]
            };
            event = await createEvent(eventData);
            const updateAccount = await Account.findById(account.account.data_id);
            updateAccount.involve.push(event.event.data_id);
            await updateAccount.save();
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Event.collection.drop();
            await Account.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should cancel event', async function(){
        const url = `/events/${event.event.data_id}/cancel`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token});

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('involve');
        data.involve.should.be.a('array');
        expect(data.involve).to.not.include(account.account.data_id.toString());
        const updatedAccount = await Account.findById(account.account.data_id);
        expect(updatedAccount.involve).to.not.include(event.event.data_id);
    });

    it('It should not cancel event when no token provided.', async function(){
        const url = `/events/${event.event.data_id}/cancel`;

        const res = await chai.request(app)
        .post(url);
        
        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });

    it('It should not cancel event when event no found.', async function(){
        const id = mongoose.Types.ObjectId();
        const url = `/events/${id.toHexString()}/cancel`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token});

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });

    it('It should not cancel event when event has passed.', async function(){
        const now = new Date();
        const newEventData = {
            'name':'New Event',
            'shortname': 'ne12',
            'start':addDays(now,-3),
            'end':addDays(now,-2),
            'description':'This is test event',
            'location':'My Home',
            'involve':account.account.data_id
        };
        const newEvent = await createEvent(newEventData);
        
        const url = `/events/${newEvent.event.data_id}/cancel`;

        const res = await chai.request(app)
        .post(url)
        .set({'Authorization':token});

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        res.body.detail_message.should.have.property('__all__');
        expect(res.body.detail_message['__all__']).to.include('Event has passed.');
    });
});