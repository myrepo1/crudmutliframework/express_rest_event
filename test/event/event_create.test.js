const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const {format,addDays} = require('date-fns');

const app = require('../../src/index');
const Account = require('../../src/models/account');
const Event = require('../../src/models/event');
const {createAccount,createToken, createEvent} = require('../test_helper');

chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('Event Create', function() {
    let account = null;
    let token = null;
    const now = new Date();
    now.setMilliseconds(0);
    now.setSeconds(0);

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
        } catch (err) {
            console.log(err);
        }
    });

    afterEach(async function(){
        try {
            await Account.collection.drop();
            await Event.collection.drop();
        } catch (err) {
            return;
        }
    });

    it('It should create event',async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'New Event',
            'shortname': 'ne1',
            'start':format(addDays(now,3),dateFormat),
            'end':format(addDays(now,4),dateFormat),
            'description':'This is test event',
            'location':'My Home'
        };
        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(201);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(201);
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('name');
        data.should.have.property('shortname');
        data.should.have.property('start');
        data.name.should.equal(payload.name);
        data.start.should.equal(payload.start);
        let is_event = false;
        const created_event = await Event.findOne({name:payload.name});
        if(created_event) {
            is_event = true;
        }
        expect(is_event).to.be.true;
    });

    it('It should not create event when no token provided', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'New Event',
            'shortname': 'ne1',
            'start':format(addDays(now,3),dateFormat),
            'end':format(addDays(now,4),dateFormat),
            'description':'This is test event',
            'location':'My Home'
        };
        const res = await chai.request(app)
        .post('/events')
        .send(payload);

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });

    it('It should not create event when using non staff account', async function(){
        const newAccountData = {
            username:'newAccount',
            password:'123123',
            name:'New Account'
        };
        const newAccount = await createAccount(newAccountData);
        const newToken = createToken(newAccount.account);
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'New Event',
            'shortname': 'ne1',
            'start':format(addDays(now,3),dateFormat),
            'end':format(addDays(now,4),dateFormat),
            'description':'This is test event',
            'location':'My Home'
        };
        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':newToken})
        .send(payload);

        res.should.have.status(403);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(403);
    });

    it('It should not create event when required field is empty', async function(){
        const payload = {
            'name':'',
            'shortname': '',
            'start':''
        };

        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('name');
        error.should.have.property('start');
        expect(error.name).to.include("'name' is not allowed to be empty");
        expect(error.start).to.include("'start' is not allowed to be empty");
    });

    it('It should not create event when data has invalid value', async function(){
        const payload = {
            'name':'New Event',
            'shortname': 'ne1',
            'start':'asdasd',
            'end':'asdasd',
            'description':'This is test event',
            'location':'My Home'
        };
        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('start');
        error.should.have.property('end');
        expect(error.start).to.include('Unable to parse value.');
        expect(error.end).to.include('Unable to parse value.');
    });

    it('It should not create event when start after end', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'New Event',
            'shortname': 'ne1',
            'start':format(addDays(now,4),dateFormat),
            'end':format(addDays(now,3),dateFormat),
            'description':'This is test event',
            'location':'My Home'
        };
        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('__all__');
        expect(error['__all__']).to.include('Start must be before end.');
    });

    it('It should not create event when event already exist', async function(){
        const event = await createEvent();
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':event.event.name,
            'shortname': 'ne1',
            'start':format(addDays(now,3),dateFormat),
            'end':format(addDays(now,4),dateFormat),
            'description':'This is test event',
            'location':'My Home'
        };

        const res = await chai.request(app)
        .post('/events')
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('name');
        expect(error.name).to.include(`Event with name ${event.event.name} already exist.`);
    });
});