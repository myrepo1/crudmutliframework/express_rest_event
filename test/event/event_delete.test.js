const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const {format,addDays} = require('date-fns');

const app = require('../../src/index');
const Account = require('../../src/models/account');
const Event = require('../../src/models/event');
const {createAccount,createToken, createEvent} = require('../test_helper');

chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('Event Delete', function(){
    let account = null;
    let token = null;
    let event = null;

    const now = new Date();
    now.setMilliseconds(0);
    now.setSeconds(0);

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
            event = await createEvent();
        } catch (err) {
            console.log(err);
        }
    });

    afterEach(async function(){
        try {
            await Account.collection.drop();
            await Event.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should delete event', async function(){
        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .delete(url)
        .set({'Authorization':token});

        res.should.have.status(200);res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
    });

    it('It should not delete event when no token provided', async function() {
        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app).delete(url);

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
    });

    it('It should not delete event when use non staff account', async function(){
        const newAccountData = {
            username:'newAccount',
            password:'123123',
            name:'New Account'
        };
        const newAccount = await createAccount(newAccountData);
        const newToken = createToken(newAccount.account);
        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .delete(url)
        .set({'Authorization':newToken});

        res.should.have.status(403);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(403);
    });

    it('It should not delete event when event is not found', async function(){
        const id = mongoose.Types.ObjectId();
        const url = `/events/${id.toHexString()}`;
        const res = await chai.request(app)
        .delete(url)
        .set({'Authorization':token});

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });
});