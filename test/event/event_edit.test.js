const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const {format,addDays} = require('date-fns');

const app = require('../../src/index');
const Account = require('../../src/models/account');
const Event = require('../../src/models/event');
const {createAccount,createToken, createEvent} = require('../test_helper');

chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('Event Edit', function(){
    let account = null;
    let token = null;
    let event = null;

    const now = new Date();
    now.setMilliseconds(0);
    now.setSeconds(0);

    beforeEach(async function() {
        try {
            account = await createAccount();
            token = createToken(account.account);
            event = await createEvent();
        } catch (err) {
            console.log(err);
        }
    });

    afterEach(async function(){
        try {
            await Account.collection.drop();
            await Event.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should edit event', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'Edited',
            'start':format(addDays(event.source.start,1),dateFormat)
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        res.body.status_code.should.equal(200);
        const data = res.body.data;
        data.should.have.property('name');
        data.should.have.property('start');
        data.name.should.equal(payload.name);
        data.start.should.equal(payload.start);
        let is_edited = false;
        const edited_event = await Event.findById(event.event.data_id);
        if(edited_event.name===payload.name) {
            is_edited = true;
        }
        expect(is_edited).to.be.true;
    });

    it('It should not edit event when no token is provided', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'Edited',
            'start':format(addDays(event.source.start,1),dateFormat)
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .send(payload);

        res.should.have.status(401);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(401);
        let is_edited = true;
        const edited_event = await Event.findById(event.event.data_id);
        if(edited_event.name!==payload.name) {
            is_edited = false;
        }
        expect(is_edited).to.be.false;
    });

    it('It should not edit event when use non staff account', async function(){
        const newAccountData = {
            username:'newAccount',
            password:'123123',
            name:'New Account'
        };
        const newAccount = await createAccount(newAccountData);
        const newToken = createToken(newAccount.account);
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'Edited',
            'start':format(addDays(event.source.start,1),dateFormat)
        };
        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':newToken})
        .send(payload);

        res.should.have.status(403);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(403);

        let is_edited = true;
        const edited_event = await Event.findById(event.event.data_id);
        if(edited_event.name!==payload.name) {
            is_edited = false;
        }
        expect(is_edited).to.be.false;
    });

    it('It should not edit event when event is not found', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'Edited',
            'start':format(addDays(event.source.start,1),dateFormat)
        };
        const id = mongoose.Types.ObjectId();
        const url = `/events/${id.toHexString()}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);
        
        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });

    it('It should not edit event when required field is empty', async function(){
        const payload = {
            'name':'',
            'start':''
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        const error = res.body.detail_message;
        error.should.have.property('name');
        error.should.have.property('start');
        expect(error.name).to.include("'name' is not allowed to be empty");
        expect(error.start).to.include("'start' is not allowed to be empty");
        
        let is_edited = true;
        const edited_event = await Event.findById(event.event.data_id);
        if(edited_event.name!==payload.name) {
            is_edited = false;
        }
        expect(is_edited).to.be.false;
    });

    it('It should not edit event when data is invalid', async function(){
        const payload = {
            'start':'asdasd',
            'end':'asdasd',
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);
        
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('start');
        error.should.have.property('end');
        expect(error.start).to.include('Unable to parse value.');
        expect(error.end).to.include('Unable to parse value.');
        let is_edited = true;
        try {
            const edited_event = await Event.findById(event.event.data_id);
            if(edited_event.start!==payload.start) {
                is_edited = false;
            }   
        } catch (err) {
            console.log(err);
        }
        expect(is_edited).to.be.false;
    });

    it('It should not edit event when start after end', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const payload = {
            'name':'Edited',
            'start':format(addDays(event.source.start,4),dateFormat)
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('__all__');
        expect(error['__all__']).to.include('Start must be before end.');

        let is_edited = true;
        try {
            const edited_event = await Event.findById(event.event.data_id);
            if(edited_event.start!==payload.start) {
                is_edited = false;
            }   
        } catch (err) {
            console.log(err);
        }
        expect(is_edited).to.be.false;
    });

    it('It should not edit when event already exist', async function(){
        const dateFormat = 'dd-MM-yyyy HH:mm';
        const newEventData = {
            name: 'More Event',
            shortname: 'me1',
            start: addDays(event.source.start,2)
        };
        await createEvent(newEventData);
        const payload = {
            name:newEventData.name
        };

        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app)
        .put(url)
        .set({'Authorization':token})
        .send(payload);

        res.should.have.status(400);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(400);
        res.body.should.have.property('detail_message');
        const error = res.body.detail_message;
        error.should.have.property('name');
        expect(error.name).to.include(`Event with name ${newEventData.name} already exist.`);

        let is_edited = true;
        const edited_event = await Event.findById(event.event.data_id);
        if(edited_event.name!==payload.name) {
            is_edited = false;
        }
        expect(is_edited).to.be.false;
    });
});