const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const {addDays} = require('date-fns');

const app = require('../../src/index');
const Event = require('../../src/models/event');
const {createEvent} = require('../test_helper');

chai.should();
chai.use(chaiHttp);

describe('Event Get All', function() {
    let event = null;

    beforeEach(async function() {
        try {
            event = await createEvent();
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Event.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get events', async function(){
        const res = await chai.request(app).get('/events');

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        const data = res.body.data;
        data.should.be.a('array');
        data[0].should.have.property('data_id');
        data[0].should.have.property('name');
        data[0].should.have.property('shortname');
        data[0].should.have.property('start');
        data[0].name.should.equal(event.event.name);
    });
});

describe('Event Get All With Query', function(){
    let event = null;
    let event1 = null;
    
    beforeEach(async function() {
        try {
            event = await createEvent();
            const moreEventData = {
                name: 'More Dummy Event',
                shortname: 'me1',
                start: addDays(event.source.start,2),
                end: addDays(event.source.end,1),
            };
            event1 = await createEvent(moreEventData);
        } catch (err) {
            console.log(err);
        }
    });

    afterEach(async function() {
        try {
            await Event.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get some event with query equal', async function(){
        const res = await chai.request(app)
        .get('/events')
        .query({
            name:event.source.name
        });
        res.should.have.status(200);
        res.should.be.json;
        const data = res.body.data;
        data.should.be.a('array');
        data.length.should.equal(1);
        data[0].should.have.property('name');
        data[0].name.should.equal(event.event.name);
    });

    it('It should get some event with query ilike', async function(){
        const res = await chai.request(app)
        .get('/events')
        .query({
            name__ilike:event.source.name
        });
        res.should.have.status(200);
        res.should.be.json;
        const data = res.body.data;
        data.should.be.a('array');
        data.length.should.equal(2);
        data[0].should.have.property('name');
        data[0].name.should.equal(event.event.name);
        data[1].should.have.property('name');
        data[1].name.should.equal(event1.source.name);
    });
});