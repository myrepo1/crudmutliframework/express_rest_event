const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../src/index');
const Event = require('../../src/models/event');
const {createEvent} = require('../test_helper');

chai.should();
chai.use(chaiHttp);

describe('Event Get One', function() {
    let event = null;

    beforeEach(async function() {
        try {
            event = await createEvent();
        } catch (err) {
            console.log(err);
        }
    });
    afterEach(async function() {
        try {
            await Event.collection.drop();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get event', async function(){
        const url = `/events/${event.event.data_id}`;
        const res = await chai.request(app).get(url);

        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.should.have.property('status');
        res.body.should.have.property('data');
        res.body.should.have.property('message');
        const data = res.body.data;
        data.should.have.property('data_id');
        data.should.have.property('name');
        data.should.have.property('shortname');
        data.should.have.property('start');
        data.name.should.equal(event.event.name);
    });

    it('It should not get event when event not found', async function(){
        const id = mongoose.Types.ObjectId();
        const url = `/events/${id.toHexString()}`;
        const res = await chai.request(app).get(url);

        res.should.have.status(404);
        res.should.be.json;
        res.body.should.have.property('status_code');
        res.body.status_code.should.equal(404);
    });
});