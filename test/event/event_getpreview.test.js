const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');


require('../../src/index');
const Event = require('../../src/models/event');
const {createEvent,clearMedia} = require('../test_helper');

chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('Event Get Preview', function(){
    let event = null;

    beforeEach(async function(){
        try {
            event = await createEvent(null,true);
        } catch (err) {
            console.log(err);
        }
    });

    afterEach(async function(){
        try {
            await Event.collection.drop();
            clearMedia();
        } catch (err) {
            console.log(err);
        }
    });

    it('It should get preview', async function(){
        const url = event.event.preview;
        const res = await chai.request(url).get('/');
        res.should.have.status(200);
        res.type.should.equal('image/jpeg');
    });
});