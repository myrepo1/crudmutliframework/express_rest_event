require('dotenv/config');
const path = require('path');
const crypto = require('crypto');
const fs = require('fs');
const {addDays} = require('date-fns');

const {hashPassword,generateToken} = require('../src/helpers/security');
const {checkDirExist,createDir,deleteDir,generateFullPath} = require('../src/helpers/file');
const Account = require('../src/models/account');
const Event = require('../src/models/event');
const { events } = require('../src/models/account');

const filesEndPoint = process.env.BASE_URL + '/files';

const promiseCopyFile = (...args) => {
    return new Promise((resolve,reject)=>fs.copyFile(...args,(err)=>{
        if(err) {
            reject(err);
        } else {
            resolve();
        }
    }));
};

const getImage = (file)=> {
    const imagePath = path.join(process.env.PWD,'src','seeder','static',`${file}.jpeg`);
    return fs.readFileSync(imagePath);
};

const clearMedia = () => {
    if(checkDirExist('')) {
        deleteDir('');
    }
}

const prepareMedia = (container) => {
    if(!checkDirExist(container)) {
        createDir(container);
    }
}

const createAccount = async (data,useProfpic=false)=>{
    const result = {};
    if(data) {
        result.source = data;
    } else {
        result.source = {
                username:'test',
                password: await hashPassword('test'),
                name:'User Dummy',
                is_staff:true
            };
    }
    if(useProfpic) {
        createDir('profpic');
        const profpicPath = path.join(process.env.PWD, 'src','seeder','static','profpic.jpeg');
        if(!checkDirExist(profpicPath)) {
            console.log('Profpic Image Not Found.');
        } else {
            const extname = profpicPath.split('.').pop();
            const fileName = crypto.randomBytes(12).toString('hex');
            await promiseCopyFile(profpicPath,generateFullPath(`profpic/${fileName}.${extname}`));
            result.source.profpic = filesEndPoint + `/profpic/${fileName}.${extname}`;
        }
    }
    try {
        const account = new Account(result.source);
        await account.save();
        result.account = account.toObject();
        return result;   
    } catch (err) {
        return result;
    }
};

const createEvent = async (data,usePreview=false)=>{
    const result = {};
    const now = new Date();
    now.setSeconds(0);
    now.setMilliseconds(0);
    if(data) {
        result.source = data;
    } else {
        result.source = {
                name:'Dummy Event',
                shortname: 'dme1',
                start: addDays(now,10),
                end: addDays(now,12),
                description:'This is dummy event',
                location: 'Earth'
            };
    }
    if(usePreview) {
        createDir('preview');
        const previewPath = path.join(process.env.PWD, 'src','seeder','static','preview.jpeg');
        if(!checkDirExist(previewPath)) {
            console.log('Preview Image Not Found.');
        } else {
            const extname = previewPath.split('.').pop();
            const fileName = crypto.randomBytes(12).toString('hex');
            await promiseCopyFile(previewPath,generateFullPath(`preview/${fileName}.${extname}`));
            result.source.preview = filesEndPoint + `/preview/${fileName}.${extname}`;
        }
    }
    try {
        const event = new Event(result.source);
        await event.save();
        result.event = event.toObject();
        return result;
    } catch (err) {
        console.log(err);
        return result;
    }
};

const createToken = (data)=>{
    const payload = {id:data.data_id,is_staff:data.is_staff};
    const token = 'Bearer ' + generateToken(payload);
    return token;
};

module.exports = {
    createAccount,
    createEvent,
    createToken,
    getImage,
    prepareMedia,
    clearMedia
};